package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Piatpol.T
 */
public class Coin extends AbstractValuable{    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     * @param currency
	 *            is a medium of exchange in specific country.
     */
    public Coin( double value , String currency) {
       	super(value,currency);
    }

    /**
     * @return Show the coin value in that currency.
     */
    public String toString()
    {
    	return (int)super.getValue()+"-"+super.getCurrency()+" Coin.";
    }   
}