package coinpurse;

import java.util.ResourceBundle;
import java.util.PropertyResourceBundle;

public abstract class MoneyFactory {
	private static MoneyFactory instance = null;
	static MoneyFactory getInstance() {
		ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
		String factoryclass = bundle.getString("purse.moneyfactory");
		String currency = bundle.getString("purse.currency");
		System.out.println("Factory class is " + factoryclass); 
		try {
			instance = (MoneyFactory)Class.forName(factoryclass).newInstance();
		} catch ( Exception ex) {
			//TODO what exceptions might be thrown?
			System.out.println("Error creating MoneyFactory "+ex.getMessage() );
			return new ThaiMoneyFactory();
		}
		return instance;

	}

	private static MoneyFactory defaultMoneyFactory() {
		return new ThaiMoneyFactory();
	}

	public abstract Valuable createMoney(double value);

	public Valuable createMoney( String value ) {
		return createMoney(Double.parseDouble(value));
	}
}
