package coinpurse;

/**
 * A BankNote with a monetary value.
 * You can't change the value of a BankNote.
 * @author Pipatpol Tanavongchinda
 * @version 2015-01-27
 */
public class BankNote extends AbstractValuable{
	/** Start serial number of the BankNote */
	static private  int serialnumber = 1000000;
	/** Serial number of the BankNote */
	private int serial;
	
	/**
	 * Constructor for a new BankNote. 
	 * @param value of BankNote
	 * @param currency
	 *            is a medium of exchange in specific country. 
	 */
	public BankNote(int value, String currency)
	{
		super(value,currency);
		serial = serialnumber;
		serialnumber++;
	}
	
	 /**
     * @return Show the "xxx-currency Banknote [serialnum]".
     */
	public String toString()
	{
		return super.getValue()+"-"+ super.getCurrency() +" BankNote ["+this.serial+"]";
	}
}
