package coinpurse;

/**
 * The abstract class that make the subclass' code shorter.
 * 
 * @author Pipatpol Tanavongchinda
 * @version 2015-02-10
 */
public abstract class AbstractValuable implements Valuable {
	/** Value of this object. */
	private double value;
	private String currency;

	/**
	 * Constructor for a AbstractValuable.
	 * 
	 * @param value
	 *            is a value of this object.
	 * @param currency
	 *            is a medium of exchange in specific country.
	 */
	public AbstractValuable(double value, String currency) {
		this.value = value;
		this.currency = currency;
	}

	/**
	 * Get the value of this object.
	 * 
	 * @return return value of this object.
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * Get the currency of this object.
	 * @return this currency of this object.
	 */
	public String getCurrency() {
		return this.currency;
	}
	/**
	 * Compare between this object and other object.
	 * 
	 * @param obj
	 *            is the other object.
	 * @return If this object's value less than other's value return -1 , if
	 *         this object's value more than other's value return 1 , if this
	 *         object's value equals other's value return 0. else cannot compare
	 *         return -1.
	 */
	public int compareTo(Object obj) {
		Valuable other = (Valuable) obj;
		if (this.getValue() < other.getValue()) {
			return -1;
		} else if (this.getValue() > other.getValue()) {
			return 1;
		} else if (this.getValue() == other.getValue()) {
			return 0;
		}
		return -1;
	}

	/**
	 * Check this object and other object have the same value.
	 * 
	 * @param obj
	 *            is the other object.
	 * @return true when this object and other object have the same value.
	 */
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Valuable other = (Valuable) obj;
		if (other.getValue() == this.getValue()) {
			return true;
		}
		return false;
	}
}
