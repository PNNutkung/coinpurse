package coinpurse;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import java.awt.Font;
import javax.swing.SwingConstants;
/**
 * This GUI shows status about Purse.
 * @author Pipatpol Tanavongchinda
 */
public class StatusGUI implements Observer{

	static JLabel balanceLabel;
	static JLabel status;
	static JProgressBar progress;
	static JFrame purseStatusContainer;
	
	/**
	 * Constructor that create GUI components.
	 * @param capacity is the maximum that purse can take.
	 */
	public StatusGUI(int capacity)
	{
		this.initComponents(capacity);
	}
	@Override
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse)
		{
			Purse purse = (Purse)subject;
			int balance = (int) purse.getBalance();
			progress.setValue(purse.count());
			if(purse.getBalance() == 0)
			{
				status.setText("Empty");
			}
			else if(purse.isFull())
			{
				status.setText("FULL.");
			}
			else 
			{
				status.setText(purse.count() + " Items.");
			}
			purseStatusContainer.pack();
		}
		if (info != null ) System.out.println(info);
	}
	/**
	 * Get all components of this GUI.
	 * @param capacity is the maximum that purse can take.
	 */
	public void initComponents(int capacity)
	{
		purseStatusContainer = new JFrame();
		purseStatusContainer.setTitle("Purse Status");
		JPanel purseStatusPanel = new JPanel();
		purseStatusPanel.setLayout(new BoxLayout(purseStatusPanel,BoxLayout.Y_AXIS));
		status = new JLabel("Empty");
		status.setHorizontalAlignment(SwingConstants.CENTER);
		status.setFont(new Font("Tahoma", Font.PLAIN, 52));
		progress = new JProgressBar(0,capacity);
		purseStatusPanel.add(status);
		purseStatusPanel.add(progress);
		purseStatusContainer.getContentPane().add(purseStatusPanel);
		purseStatusContainer.setVisible(true);
		purseStatusContainer.pack();
		purseStatusContainer.setDefaultCloseOperation(purseStatusContainer.EXIT_ON_CLOSE);
	}




}
