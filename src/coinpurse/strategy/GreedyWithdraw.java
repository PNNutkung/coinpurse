package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Purse;
import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * Greedy withdraw that use Greedy algorithm pull out the most valuable object first 
 * and pull out the rest by descending value you pull out.
 * @author Pipatpol Tannavongchinda
 *
 */
public class GreedyWithdraw implements WithdrawStrategy{
	private static ValueComparator comparator = new ValueComparator();
	/**
	 * Withdraw money by use Greedy algorithm.
	 * @param amount is the money that you want to pick up. 
	 * @param money is your purse.
	 * @return List of money that you want to pick up.
	 */
	public ArrayList<Valuable> withdraw(double amount,List<Valuable> money)
	{

        if(amount <= 0)
        {
        	return null;
        }
	   
		if ( amount > 0 )
		{
			
			Collections.sort(money , comparator);
			ArrayList<Valuable> out_Valuable = new ArrayList<Valuable>();
			ArrayList<Valuable> clone = new ArrayList<Valuable>();
			for(int run = 0;run<money.size();run++)
			{
				clone.add(money.get(run));
			}
			for(int count = money.size()-1;count>=0;count--)
			{
				if(money.get(count).getValue()<=amount)
				{
					amount -= money.get(count).getValue();
					out_Valuable.add(money.get(count));
				}
			}
			if(0 != amount)
			{
				return null;
			}
			return out_Valuable;
		}
        return null;
	}
}
