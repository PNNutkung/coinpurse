package coinpurse.strategy;

import java.util.List;
import coinpurse.Valuable;
/**
 * The Withdraw strategy that perform withdraw from GreedyWithdraw or RecursiveWithdraw.
 * @author Pipatpol Tanavongchinda
 *
 */
public interface WithdrawStrategy {
	public List<Valuable> withdraw(double amount,List<Valuable> valuables);
	
}
