package coinpurse;


import java.util.Hashtable;
import java.util.Map;

/**
 * A coupon with a monetary value and colour.
 * You can't change the value of a coupon.
 * @author Pipatpol Tanavongchinda
 * @version 2015-01-27
 */
public class Coupon extends AbstractValuable{
	/** Value of Coupon. */
	//private double value;
	/** Colour of Coupon. */
	private String color_of_Coupon;
	/** Colour map of Coupon. */
	static Map<String,Double> colour;
	
	static {
		colour = new Hashtable<String,Double>();
		colour.put("red", new Double(100));
		colour.put("blue", new Double(50));
		colour.put("green", new Double(20));
	}
	
	
	/**
	 * Constructor for a new coupon. 
	 * @param color of Coupon 
	 */
	public Coupon(String color,String currency)
	{
		super(colour.get(color.toLowerCase()),currency);
		color_of_Coupon = color;		
	}
	
	/**
	 * @return describing the Coupon.
	 */
	public String toString()
	{
		return color_of_Coupon+" coupon";
	}
	
}
