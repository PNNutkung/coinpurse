package coinpurse;

public class ThaiMoneyFactory extends MoneyFactory {

	@Override
	public Valuable createMoney(double value) {
		if( value <= 10 && value > 1) return new Coin(value, "Baht");
		return new BankNote((int)value, "Baht");
	}
	
}
