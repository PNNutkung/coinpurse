package coinpurse;



/**
 * Interface that get the value of object.
 * @author Pipatpol Tanavongchinda
 * @version 2015-01-27
 */
public interface Valuable extends Comparable{
	/**
	 * Get the value of this object.
	 * @return the value of this object.
	 */
	public double getValue( );

}
