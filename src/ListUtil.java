import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class ListUtil {
//	List food = Arrays.asList( "apple","banana","grape","fig");

	private static void printList(List<?> list)
	{ 
		if(list.size()>1){
			System.out.print(list.get(0)+",");
			printList(list.subList(1, list.size()));
		}
		else if (list.size() == 1)System.out.println(list.get(list.size()-1));
	}
	private static String max( List<String> list)
	{
		if(list.size()==1)
		{
			return list.get(0);
		}
		if(list.get(0).compareTo(list.get(1)) > 0)
		{
			return list.get(0);
		}
		String getMax = max(list.subList(1, list.size()));
		return getMax;
		
	}
	public static void main(String[] args)
	{
		List<String> list;
		
		if(args.length > 0) list = Arrays.asList( args );
		else list = Arrays.asList("bird","zebra","cat","pig");
		
		System.out.print("List contains: ");
		printList( list );
		String max = max(list);
		System.out.println("Lexically greatest element is "+max);
	}
}
