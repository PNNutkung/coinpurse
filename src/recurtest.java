
public class recurtest {
	public static double sum(double[] x)
	{
		return sumOf(x,x.length-1);
	}
	private static double sumOf(double[] arr, int lastIndex)
	{
		if(lastIndex<0) return 0.0;
		return arr[lastIndex]+sumOf(arr,lastIndex-1);
	}
	public static void main(String[] args)
	{
		double[] arr = new double []{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
		System.out.println(sum(arr));
	}
}
